/* eslint-disable no-undef */
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');
const apiRouter = require('../apiRouter').router;

const server = express();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use('/api/', apiRouter);

describe('POST /users/register/', () => {
  describe('given good credentials to register', () => {
    // should save the credentials to the database
    // should respond with a json object containg the user id
    it('should respond with a 201 status code', async () => {
      const response = await request(server).post('/api/users/register/').send({
        username: 'test',
        email: 'test@test.com',
        bio: 'Text test',
        password: '123test',
      });
      expect(response.statusCode).toBe(201);
    });
  });
});

describe('POST /users/login/', () => {

});

describe('GET /users/me/', () => {

});

describe('PUT /users/me/', () => {

});

describe('GET /users/user/:userId', () => {

});
