module.exports = (sequelize, DataTypes) => {
  const Message = sequelize.define('Message', {
    title: DataTypes.STRING,
    content: DataTypes.STRING,
    attachement: DataTypes.STRING,
    likes: DataTypes.INTEGER,
  }, {
    classMethods: {
      associate: (models) => {
        // define association here
        models.Message.belongsTo(models.User, {
          foreignkey: {
            allowNull: false,
          },
        });
      },
    },
  });
  return Message;
};
