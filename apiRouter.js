// Imports
const express = require('express');
const usersCtrl = require('./routes/usersController');
const messagesCtrl = require('./routes/messagesContoller');
const likesCtrl = require('./routes/likesController');

// Routes
exports.router = (() => {
  const apiRouter = express.Router();

  // Users routes
  apiRouter.route('/users/register/').post(usersCtrl.register);
  apiRouter.route('/users/login').post(usersCtrl.login);
  apiRouter.route('/users/me').get(usersCtrl.getMyUserProfile);
  apiRouter.route('/users/me').put(usersCtrl.updateUserProfile);
  apiRouter.route('/users/user/:userId').get(usersCtrl.getUserProfile);

  // Messages routes
  apiRouter.route('/messages/new').post(messagesCtrl.createMessage);
  apiRouter.route('/messages/').get(messagesCtrl.listMessage);
  apiRouter.route('/messages/:messageId/delete').post(messagesCtrl.deleteMessage);

  // Likes routes
  apiRouter.route('/messages/:messageId/vote/like').post(likesCtrl.likePost);
  apiRouter.route('/messages/:messageId/vote/dislike').post(likesCtrl.dislikePost);

  return apiRouter;
})();
