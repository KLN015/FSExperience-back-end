/* eslint-disable consistent-return */
// Imports
const asyncLib = require('async');
const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');

// Constants
const TITLE_LIMIT = 2;
const CONTENT_LIMIT = 4;

// Routes
module.exports = {
  createMessage: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }

    const { title } = req.body;
    const { content } = req.body;

    if (title == null || content == null) {
      return res.status(400).json({ error: 'missing parameters' });
    }

    if (title.length <= TITLE_LIMIT || content.length <= CONTENT_LIMIT) {
      return res.status(400).json({ error: 'invalid parameters' });
    }

    asyncLib.waterfall([
      (done) => {
        models.User.findOne({
          where: { id: userId },
        })
          .then((_userFound) => {
            done(null, _userFound);
          })
          .catch(() => res.staus(500).json({ error: 'unable to verify user' }));
      },
      (_userFound, done) => {
        if (_userFound) {
          models.Message.create({
            title,
            content,
            likes: 0,
            UserId: _userFound.id,
          })
            .then((_newMessage) => {
              done(_newMessage);
            });
        } else {
          return res.status(404).json({ error: 'user not found' });
        }
      },
    ], (_newMessage) => {
      if (_newMessage) {
        return res.status(201).json(_newMessage);
      }
      return res.status(500).json({ error: 'cannot post message' });
    });
  },
  listMessage: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }

    const { fields } = req.body;
    const limit = parseInt(req.body.limit, 10);
    const offset = parseInt(req.body.offset, 10);
    const { order } = req.body;

    models.Message.findAll({
      order: [(order != null) ? order.split(':') : ['title', 'ASC']],
      attributes: (fields !== '*' && fields != null) ? fields.split(',') : null,
      limit: (!Number.isNaN(limit)) ? limit : null,
      offset: (!Number.isNaN(offset)) ? offset : null,
      include: [{
        model: models.User,
        attributes: ['username'],
      }],
    })
      .then((_messages) => {
        if (_messages) {
          return res.status(200).json(_messages);
        }
        return res.status(404).json({ error: 'no messages found' });
      })
      .catch(() => res.status(500).json({ error: 'invalid fields' }));
  },
  deleteMessage: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }

    const messageId = parseInt(req.params.messageId, 10);

    models.Message.destroy({
      where: {
        id: messageId,
        UserId: userId,
      },
    })
      .then(() => res.status(200).json({ message: 'message deleted' }))
      .catch(() => res.status(404).json({ error: 'cannot find this message' }));
  },
};
