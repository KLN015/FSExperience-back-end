/* eslint-disable consistent-return */
// Imports
const bcrypt = require('bcrypt');
const asyncLib = require('async');
const jwtUtils = require('../utils/jwt.utils');
const models = require('../models');

// Constants
// eslint-disable-next-line no-useless-escape
const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const PASSWORD_REGEX = /^(?=.*\d).{4,8}$/;
const USERNAME_MIN_LIMIT = 3;
const USERNAME_MAX_LIMIT = 13;

// Routes
module.exports = {
  register: (req, res) => {
    // Params
    const { email } = req.body;
    const { username } = req.body;
    const { password } = req.body;
    const { bio } = req.body;

    if (email == null || username == null || password == null) {
      return res.status(400).json({ error: 'missing parameters' });
    }

    if (username.length >= USERNAME_MAX_LIMIT || username.length < USERNAME_MIN_LIMIT) {
      return res.status(400).json({ error: 'wrong username (must be length 3 - 12)' });
    }
    if (!EMAIL_REGEX.test(email)) {
      return res.status(400).json({ error: 'email is not valid' });
    }
    if (!PASSWORD_REGEX.test(password)) {
      return res.status(400).json({ error: 'password invalid (must length 4 - 8 and include 1 number at)' });
    }

    asyncLib.waterfall([
      (done) => {
        models.User.findOne({
          attributes: ['email'],
          where: { email },
        })
          .then((_userFound) => {
            done(null, _userFound);
          })
          .catch(() => res.status(500).json({ error: 'unable to verify user' }));
      },
      (_userFound, done) => {
        if (!_userFound) {
          bcrypt.hash(password, 5, (err, bcryptedPassword) => {
            done(null, _userFound, bcryptedPassword);
          });
        } else {
          return res.status(409).json({ error: 'user already exist' });
        }
      },
      (_userFound, bcryptedPassword, done) => {
        models.User.create({
          email,
          username,
          password: bcryptedPassword,
          bio,
          isAdmin: 0,
        })
          .then((_newUser) => {
            done(_newUser);
          })
          .catch(() => res.status(500).json({ error: 'cannot add user' }));
      },
    ], (_newUser) => {
      if (_newUser) {
        return res.status(201).json({
          userId: _newUser.id,
          token: jwtUtils.generateTokenForUser(_newUser),
        });
      }
      return res.status(500).json({ error: 'cannot add user' });
    });
  },
  login: (req, res) => {
    // Params
    const { email } = req.body;
    const { password } = req.body;

    if (email == null || password == null) {
      return res.status(400).json({ error: 'missing parameters' });
    }

    if (!EMAIL_REGEX.test(email)) {
      return res.status(400).json({ error: 'email is not valid' });
    }
    if (!PASSWORD_REGEX.test(password)) {
      return res.status(400).json({ error: 'password invalid (must length 4 - 8 and include 1 number at)' });
    }

    asyncLib.waterfall([
      (done) => {
        models.User.findOne({
          where: { email },
        })
          .then((_userFound) => {
            done(null, _userFound);
          })
          .catch(() => res.status(500).json({ error: 'unable to verify user' }));
      },
      (_userFound, done) => {
        if (_userFound) {
          bcrypt.compare(password, _userFound.password, (errBcrypt, resBcrypt) => {
            done(null, _userFound, resBcrypt);
          });
        } else {
          return res.status(404).json({ error: 'user not exist in DB' });
        }
      },
      (_userFound, resBcrypt, done) => {
        if (resBcrypt) {
          done(_userFound);
        } else {
          return res.status(403).json({ error: 'invalid password' });
        }
      },
    ], (_userFound) => {
      if (_userFound) {
        return res.status(200).json({
          userId: _userFound.id,
          token: jwtUtils.generateTokenForUser(_userFound),
        });
      }
      return res.status(500).json({ error: 'cannot log on user' });
    });
  },
  getMyUserProfile: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }
    models.User.findOne({
      attributes: ['id', 'email', 'username', 'bio'],
      where: { id: userId },
    }).then((_user) => {
      if (_user) {
        return res.status(200).json(_user);
      }
      return res.status(404).json({ error: 'user not found' });
    }).catch(() => res.status(500).json({ error: 'cannot fetch user' }));
  },
  getUserProfile: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }

    const otherUserId = parseInt(req.params.userId, 10);

    models.User.findOne({
      attributes: ['username', 'bio'],
      where: { id: otherUserId },
    }).then((_user) => {
      if (_user) {
        return res.status(200).json(_user);
      }
      return res.status(404).json({ error: 'user not found' });
    }).catch(() => res.status(500).json({ error: 'cannot fetch user' }));
  },
  updateUserProfile: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    if (userId < 0) {
      return res.status(400).json({ error: 'wrong token' });
    }

    const { bio } = req.body;

    asyncLib.waterfall([
      (done) => {
        models.User.findOne({
          attributes: ['id', 'bio'],
          where: { id: userId },
        }).then((_userFound) => {
          done(null, _userFound);
        }).catch(() => res.Status(500).json({ error: 'unable to verify user' }));
      },
      (_userFound, done) => {
        if (_userFound) {
          _userFound.update({
            bio: (bio || _userFound.bio),
          }).then(() => {
            done(_userFound);
          }).catch(() => res.status(500).json({ error: 'cannot update user' }));
        } else {
          return res.status(404).json({ error: 'user not found' });
        }
      },
    ], (_userFound) => {
      if (_userFound) {
        return res.status(201).json(_userFound);
      }
      return res.status(500).json({ error: 'cannot update user profile' });
    });
  },
};
