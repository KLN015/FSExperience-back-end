/* eslint-disable consistent-return */
// Imports
const asyncLib = require('async');
const models = require('../models');
const jwtUtils = require('../utils/jwt.utils');

// Constants
const DISLIKED = 0;
const LIKED = 1;

// Routes
module.exports = {
  likePost: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    const messageId = parseInt(req.params.messageId, 10);

    if (messageId <= 0) {
      return res.status(400).json({ error: 'invalid parameters' });
    }

    asyncLib.waterfall([
      (done) => {
        models.Message.findOne({
          where: { id: messageId },
        })
          .then((_messageFound) => {
            done(null, _messageFound);
          })
          .catch(() => res.status(500).json({ error: 'unable to verify message' }));
      },
      (_messageFound, done) => {
        if (_messageFound) {
          models.User.findOne({
            where: { id: userId },
          })
            .then((_userFound) => {
              done(null, _messageFound, _userFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to verify user' }));
        } else {
          res.status(404).json({ error: 'post already liked' });
        }
      },
      (_messageFound, _userFound, done) => {
        if (_userFound) {
          models.Like.findOne({
            where: {
              userId,
              messageId,
            },
          })
            .then((_userAlreadyLikedFound) => {
              done(null, _messageFound, _userFound, _userAlreadyLikedFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to verify is user already liked' }));
        } else {
          res.status(404).json({ error: 'user not exist' });
        }
      },
      (_messageFound, _userFound, _userAlreadyLikedFound, done) => {
        if (!_userAlreadyLikedFound) {
          _messageFound.addUser(_userFound, { isLiked: LIKED })
            .then(() => {
              done(null, _messageFound, _userFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to set user reaction' }));
        } else if (_userAlreadyLikedFound.isLiked === DISLIKED) {
          _userAlreadyLikedFound.update({
            isLiked: LIKED,
          }).then(() => {
            done(null, _messageFound, _userFound);
          }).catch(() => {
            res.status(500).json({ error: 'cannot update user reaction' });
          });
        } else {
          res.status(409).json({ error: 'message already liked' });
        }
      },
      (_messageFound, _userFound, done) => {
        _messageFound.update({
          likes: _messageFound.likes + 1,
        }).then(() => {
          done(_messageFound);
        }).catch(() => {
          res.status(500).json({ error: 'cannot update message like counter' });
        });
      },
    ], (_messageFound) => {
      if (_messageFound) {
        return res.status(201).json(_messageFound);
      }
      return res.status(500).json({ error: 'cannot update message' });
    });
  },
  dislikePost: (req, res) => {
    const headerAuth = req.headers.authorization;
    const userId = jwtUtils.getUserId(headerAuth);

    const messageId = parseInt(req.params.messageId, 10);

    if (messageId <= 0) {
      return res.status(400).json({ error: 'invalid parameters' });
    }

    asyncLib.waterfall([
      (done) => {
        models.Message.findOne({
          where: { id: messageId },
        })
          .then((_messageFound) => {
            done(null, _messageFound);
          })
          .catch(() => res.status(500).json({ error: 'unable to verify message' }));
      },
      (_messageFound, done) => {
        if (_messageFound) {
          models.User.findOne({
            where: { id: userId },
          })
            .then((_userFound) => {
              done(null, _messageFound, _userFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to verify user' }));
        } else {
          res.status(404).json({ error: 'post already liked' });
        }
      },
      (_messageFound, _userFound, done) => {
        if (_userFound) {
          models.Like.findOne({
            where: {
              userId,
              messageId,
            },
          })
            .then((_userAlreadyLikedFound) => {
              done(null, _messageFound, _userFound, _userAlreadyLikedFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to verify is user already liked' }));
        } else {
          res.status(404).json({ error: 'user not exist' });
        }
      },
      (_messageFound, _userFound, _userAlreadyLikedFound, done) => {
        if (!_userAlreadyLikedFound) {
          _messageFound.addUser(_userFound, { isLiked: DISLIKED })
            .then(() => {
              done(null, _messageFound, _userFound);
            })
            .catch(() => res.status(500).json({ error: 'unable to set user reaction' }));
        } else if (_userAlreadyLikedFound.isLiked === LIKED) {
          _userAlreadyLikedFound.update({
            isLiked: DISLIKED,
          }).then(() => {
            done(null, _messageFound, _userFound);
          }).catch(() => {
            res.status(500).json({ error: 'cannot update user reaction' });
          });
        } else {
          res.status(409).json({ error: 'message already disliked' });
        }
      },
      (_messageFound, _userFound, done) => {
        _messageFound.update({
          likes: _messageFound.likes - 1,
        }).then(() => {
          done(_messageFound);
        }).catch(() => {
          res.status(500).json({ error: 'cannot update message like counter' });
        });
      },
    ], (_messageFound) => {
      if (_messageFound) {
        return res.status(201).json(_messageFound);
      }
      return res.status(500).json({ error: 'cannot update message' });
    });
  },
};
