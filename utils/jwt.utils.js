// Imports
const jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = 'holcolpdwbqinr7g3wqq831cqpysec7baoimt14m';

// Exported function
module.exports = {
  generateTokenForUser: (_userData) => jwt.sign(
    {
      userId: _userData.id,
      isAdmin: _userData.isAdmin,
    },
    JWT_SIGN_SECRET,
    {
      expiresIn: '3h',
    },
  ),
  parseAuthorization: (_authorization) => ((_authorization != null) ? _authorization.replace('Bearer ', '') : null),
  getUserId: (_authorization) => {
    let userId = -1;
    const token = module.exports.parseAuthorization(_authorization);

    if (token != null) {
      try {
        const jwtToken = jwt.verify(token, JWT_SIGN_SECRET);
        if (jwtToken != null) {
          userId = jwtToken.userId;
        }
      } catch (_err) { /* empty */ }
    }
    return userId;
  },
};
